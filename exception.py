# num = [2,3,4,5]

# try:
#     print(num[5])
    

# except LookupError as e:
#     print(f'{e}')
    
# print("hello guys ")
numbers = [1, 2, 3, 4, 5]

try:
    print(numbers[100])  # <- Out of range index
except IndexError:   # this will execute as it is subclass of lookup
    print("The requested index is out of range")
except LookupError:
    print("Could not retrieve that value.")
