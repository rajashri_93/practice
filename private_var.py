class Car:
    def __init__(self, make, model):
        self._make = make   # Protected attribute
        self.__model = model # Private attribute
        print(self.__model)
    def get_model(self):
        return self.__model

    def set_model(self, model):
        self.__model = model

my_car = Car("Toyota", "Camry")
print(my_car.get_model())
my_car.set_model("Corolla")
print(my_car.get_model())
print(my_car.__model)

# we cannot use __private varables outside the class
