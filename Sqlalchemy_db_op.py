# 
from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:admin123@localhost:5432/flaskEx'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

class Department(db.Model):
    __tablename__ = 'departments'
    
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(100), unique=True, nullable=False)
    employees = db.relationship('Employee', back_populates='department')

class Employee(db.Model):
    __tablename__ = 'employees'
    __table_args__ = {'extend_existing': True}
    
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    address = db.Column(db.String(150), nullable=False)
    department_id = db.Column(db.Integer, db.ForeignKey('departments.id'), nullable=False)
    department = db.relationship('Department', back_populates='employees')
    salaries = db.relationship('Salary', back_populates='employee')

class Salary(db.Model):
    __tablename__ = 'salaries'
    id = db.Column(db.Integer, primary_key=True)
    amount = db.Column(db.Float, nullable=False)
    employee_id = db.Column(db.Integer, db.ForeignKey('employees.id'), nullable=False)
    employee = db.relationship('Employee', back_populates='salaries')

# defining Routes here-----

@app.route('/create_dept', methods=['POST'])
def create_dept():
    data = request.json
    print(data)
    if data is not None:  
        try:
            deptObj = Department(name=data['name'])
            db.session.add(deptObj)
            db.session.commit()
            return {"status": "Success", "message": "Data added successfully"}
        except Exception as e:
            db.session.rollback() 
            print(e)
            return {"status": "Failed", "message": f"Error in adding data: {str(e)}"}, 505
    else:
        return {"message": "Data needs to be passed"}
    

@app.route('/create_emp', methods=['POST'])
def create_emp():
    data = request.json
    print(data)
    if data is not None:  
        try:
            empObj = Employee(**data)
            db.session.add(empObj)
            db.session.commit()
            return {"status": "Success", "message": "Data added successfully"}
        except Exception as e:
            db.session.rollback() 
            print(e)
            return {"status": "Failed", "message": f"Error in adding data: {str(e)}"}, 505
    else:
        return {"message": "Data needs to be passed"}
    
@app.route('/create_sal', methods=['POST'])
def create_sal():
    data = request.json
    print(data)
    if data is not None:  
        try:
            salObj = Salary(**data)
            db.session.add(salObj)
            db.session.commit()
            return {"status": "Success", "message": "Data added successfully"}
        except Exception as e:
            db.session.rollback() 
            print(e)
            return {"status": "Failed", "message": f"Error in adding data: {str(e)}"}, 505
    else:
        return {"message": "Data needs to be passed"}
    
@app.route('/get_emp_by_dept')
def get_emp_by_dept():
    record = db.session.query(Employee.id, Employee.name, Department.name).join(Department).order_by(Department.name.desc()).all()
    employees = []
    for emp_id, emp_name, dept_name in record:
        employees.append({
            'id': emp_id,
            'name': emp_name,
            'department_name': dept_name
        })
    return jsonify({"result":employees})

# this functions will given the highest salaried employee name 
@app.route('/get_max_sal_emp')
def get_max_sal():
    max_salary_record = db.session.query(Employee.name,Salary.amount,Department.name).join(Salary).join(Department, Employee.department_id == Department.id).order_by(Salary.amount.desc()).first()
    print(max_salary_record)
    if max_salary_record:
        rec ={
                "max_salary_employee":max_salary_record[0],
                "max_salary_amount":max_salary_record[1],
                "max_salary_dept":max_salary_record[2],
            }
    return {"data":rec}
    

if __name__ == '__main__':
    with app.app_context():
        db.create_all()  # Create the tables in the database
    app.run(debug=True)
